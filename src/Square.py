import random, tkinter, time, pygame
from tkinter import messagebox

# Constants
BLACK = (0,0,0)
WHITE = (255,255,255)
RED   = (255,0,0)
BLUE  = (0,64,255)
WIN_WIDTH   = 800
WIN_HEIGHT  = 600
# Global variables
prey_list           = pygame.sprite.Group()     # Create Sprite group
all_sprites_list    = pygame.sprite.Group()     # Create Sprite group
hunt = None
t0, t1 = None, None

class PreyBlocks(pygame.sprite.Sprite):
    def __init__(self, block_color):
        super().__init__()
        self.image  = pygame.Surface([40,40])
        self.image.fill(WHITE)
        self.image.set_colorkey(WHITE)
        pygame.draw.rect(self.image,block_color,[0,0,40,40])
        self.rect = self.image.get_rect()
class HunterBlocks(pygame.sprite.Sprite):
    def __init__(self, block_color):
        super().__init__()
        self.image = pygame.image.load("hunter1.jpeg").convert()
        self.image = pygame.transform.scale(self.image,(60,60))
        self.image.set_colorkey(WHITE)
        self.rect = self.image.get_rect()
def createGamePieces(hunter=None):
    global prey_list
    global all_sprites_list
    global hunt
    if hunter is None:
        for i in range(50):
            prey = PreyBlocks(RED)
            prey.rect.x = random.randrange(0,WIN_WIDTH-40)
            prey.rect.y = random.randrange(0,WIN_HEIGHT-40)
            prey_list.add(prey)
            all_sprites_list.add(prey)
    else:
        hunt = HunterBlocks(BLUE)
        all_sprites_list.add(hunt)
def gameLogic():
    global t0,t1
    t0 = time.process_time()
    continue_game = True
    while continue_game:
        for event in pygame.event.get():
            if(event.type == pygame.QUIT):
                continue_game = False
        #Get mouse position and move hunter
        pos = pygame.mouse.get_pos()
        hunt.rect.x = pos[0]
        hunt.rect.y = pos[1]
        # Check for Sprite collision
        collision_hit_list = pygame.sprite.spritecollide(hunt,prey_list,True)
        for i in collision_hit_list:
            t1 = time.process_time()
        # FPS
        pygame.time.Clock().tick(30)
        # Draw all Sprites
        all_sprites_list.draw(game_window)
        # Refresh root window
        pygame.display.flip()
        game_window.fill(WHITE)
def showMessage():
    my_window = tkinter.Tk()
    my_window.withdraw()        # Hide main window
    messagebox.showinfo("Elapsed Time",t1 - t0)

# Initialize Pygame
pygame.init()
# Screen object attributes
game_window = pygame.display.set_mode([WIN_WIDTH,WIN_HEIGHT])
pygame.display.set_caption("Squares")
# Create game characters
createGamePieces()
createGamePieces(hunter="yes")
# Start game logic
gameLogic()
# My game exit message
showMessage()
# End game
pygame.quit()
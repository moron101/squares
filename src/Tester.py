import pygame, random, time, tkinter
from tkinter import messagebox

BLACK = (0,0,0)
WHITE = (255,255,255)
RED   = (255,0,0)
BLUE  = (0,64,255)
WIN_WIDTH   = 800
WIN_HEIGHT  = 600

class Blocks(pygame.sprite.Sprite):
    def __init__(self,block_color):
        super().__init__()
        self.image = pygame.Surface([40,40])
        self.image.fill(WHITE)
        self.image.set_colorkey(WHITE)
        pygame.draw.rect(self.image,block_color,[0,0,40,40])
        self.rect = self.image.get_rect()
def showMessage():
    my_window = tkinter.Tk()
    my_window.withdraw()        # Hide main window
    messagebox.showinfo("Elapsed Time",t1 - t0)

pygame.init()
game_window = pygame.display.set_mode([WIN_WIDTH,WIN_HEIGHT])
####################################################
block_list = pygame.sprite.Group()
all_sprites_list = pygame.sprite.Group()
for i in range(50):
    prey = Blocks(RED)
    prey.rect.x = random.randrange(0,WIN_WIDTH-40)
    prey.rect.y = random.randrange(0,WIN_HEIGHT-40)
    block_list.add(prey)
    all_sprites_list.add(prey)

hunter = Blocks(BLUE)
all_sprites_list.add(hunter)
###################################################
t0 = time.process_time()
continue_game = True
while continue_game:
    for event in pygame.event.get():
        if(event.type == pygame.QUIT):
            continue_game = False

    pygame.time.Clock().tick(30)

    pos = pygame.mouse.get_pos()
    hunter.rect.x = pos[0]
    hunter.rect.y = pos[1]

    blocks_hit_list = pygame.sprite.spritecollide(hunter,block_list,True)
    if(len(blocks_hit_list) == 0):
        t1 = time.process_time()
    all_sprites_list.draw(game_window)
    pygame.display.flip()
    game_window.fill(WHITE)
showMessage()
pygame.quit()